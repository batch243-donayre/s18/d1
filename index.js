// console.log("Good afternoon");

// Function
	// Function in Javascript are lines blocks of codes that tells our device/application to perform a certain task when called/invoke
	// Function are mostly created to create complicated tasks to run several lines of  codes in succession.
	// They are also used to prevent repeating lines/blocks of code that contains the same task/function.

	// We also learned in the previous session that we can gather data from user using promt window.
/*
	function printInput(){
		let nickname = prompt("Enter your nickname:");
		console.log("Hi " + nickname);
	}
		printInput();*/

	// However, for some use cases, this ay not be ideal
	// For other cases, functions can also process data directly passed into it instead of relying on Global variable and prompt()


// Parameters and arguments 

	// conside this function

		// (name) - paramater - it acts as named variable/otainer that exist only inside of a function.
		// (name = "Noname") - set a default value

	function printName(name = "Noname"){
		console.log("My name is" +name);
	}
		// "Chris" - argument (argument passing or initialization) 
		printName("Chris");
		printName();



	// you can directly pass data into the function. The function can then use that data which is referred as as "name" within the function.

	// values passed when invoking a function are called arguments. These arguments are then stored as the parameters within the function.

	// variable can also be passed as an argument.
		let sampleVariable = "Edward";
		printName(sampleVariable);

	// function arguments cannot be used by a function if there are no parameters proivided within the function.

		function noParams(){
			let params = "No parameter";

			console.log(params);
		}

		noParams("with parameter!");

		// === strict equality operator (return true or false)
		function checkDivisibilityBy8(num) {
			let modulo = num%8;
			console.log("The remainder of num " + num + " divide by 8 is: " +modulo);

			let isDivisibleBy8 = modulo === 0;
			console.log("Is "+num+ " divisible by 8?");
			console.log(isDivisibleBy8);

		}
		checkDivisibilityBy8(8);
		checkDivisibilityBy8(17);

		// you can also do the same using the prompt(), take note that prompt() outputs a string. strings are not ideal for mathematicals computations.

	// Function as Arguments
		// function parameters can also accept other function as arguments.
		// some complex functions as arguments to perform more complicated results.
		// This will be further seen when we discuss arrays methods.

		// argumentFunction - pass argument as function
		function argumentFunction(){
			console.log("This function was passed as an argument before the message was preinted.");
		}
		function argumentFunctionTwo(){
			console.log("This function was passed as an argument before the message from the second argument function.");
		}


		function invokeFunction(argFunction) {
			argFunction();
		}

		invokeFunction(argumentFunction);
		invokeFunction(argumentFunctionTwo);

		// Adding and removing the parethesis "()" the output of Javascript heavily.
		// when function is used with parenthesis "()", it denotes invoking a function.
		// A function used wihout parenthesis "()" is normally associated with using the function as an argument to another function.

	// Using Multiple parameters
		// 

		function createFullName(firstName = "noFirstName", middleName = "noMiddleName" , lastName ="noLastName" ){
			console.log("This is firstName:" +firstName);
			console.log("This is firstName:" +middleName);
			console.log("This is firstName:" +lastName);
		}
		createFullName("Juan","Dela","Cruz");
		//   "Juan" will be stored in the parameter "firstname"
		// succeding

		// In JavaScript, providing more arguments than the expected parameters will no return an error.
		createFullName("Juan","Dela","Cruz","Jr.");
		// Providing less arguments than the expected parameters will automatically
		createFullName("Juan",undefined,"Dela Cruz");

		// using variables are arguments.

		let firstName = "John";
		let middleName = "Doe";
		let lastName = "Smith";

		createFullName(firstName,middleName,lastName);


// return statement (console - wala narestore na data while return keyword kung ano yung )
// The "return" statement allows us to output a vaue from a function to be passed to the line/block of code that invoke the function.

		function returnFullName(firstName, middleName, LastName){
			console.log(firstName + " " + middleName + " " + lastName);
		}
		returnFullName("Ada", "None", "Lovelace");

		function returnName(firstName, middleName, LastName){
			return firstName + " " + middleName + " " + lastName
		}
		console.log(returnName("John", "Doe", "Smith"));
		let fullName = returnName("John", "Doe", "Smith");
		console.log("this is console.log from fullname variable" );
		console.log(fullName);

		function printPlayerInfo(userName, level, job){
			console.log("Username: " +userName);
			console.log("Level: " +level);
			console.log("Job: " +job);

			return userName + " " + " " +job;
		}
		printPlayerInfo("Knight_white", 95 , "Paladin");
		let user1 = printPlayerInfo("Knight_white", 95 , "Paladin");
		console.log(user1);